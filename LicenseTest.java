//Referenced the "extends Thread" code found here: https://www.tutorialspoint.com/java/java_multithreading.htm
//Compile with: javac LicenseTest.java
//Run with: java LicenseTest
class LicenseTest
{
    public static void main(String[] args)
    {
		Manager manager = new Manager();
		Program P1 = new Program("P1", manager);
		Program P2 = new Program("P2", manager);
		Program P3 = new Program("P3", manager);
		Program P4 = new Program("P4", manager);
		Program P5 = new Program("P5", manager);
		Program P6 = new Program("P6", manager);
		P1.start();
		P2.start();
		P3.start();
		P4.start();
		P5.start();
		P6.start();
    }
}

class Program extends Thread
{
	private Thread thread;
	private String name;
	private Manager manager;
	private int count;
	
	public Program(String n, Manager m) //Constructor
	{
		name = n;
		manager = m;
	}
	
	public void run()
	{
		System.out.println("Thread " + name + ": Started");
		
		try
		{
			//run the program
			count = manager.decreaseCount(1, name);
			if(count == -1)
			{
				System.out.println("Thread " + name + ": Blocked");
			}
			Thread.sleep(3000); //Simulate running for 3 seconds
		}
		catch(InterruptedException error)
		{
			System.out.println("Thread " + name + ": Interrupted");
		}
		//Thread went all the way through -- completed
		System.out.println("Thread " + name + ": Finished");
		//Release the software license
		manager.increaseCount(1);
	}
}

class Manager
{
	public static final int MAX_RESOURCES = 5;
	private int availableResources = MAX_RESOURCES;
	
	public synchronized int decreaseCount(int count, String name)
	{
		if(availableResources < count)
		{
			try
			{
				System.out.println("Thread " + name + ": Waiting");
				this.wait();
			}
			catch(InterruptedException error){}; //Literally just pass
		}
		
		availableResources -= count; //Critical section
		return 0;
	}
	
	public synchronized void increaseCount(int count)
	{
		availableResources += count; //Critical section
		notifyAll();
	}
}

//Original Manager code from Operating System Concepts with Java: problem 6.39
/* class Manager
{
	public static final int MAX_RESOURCES = 5;
	private int availableResources = MAX_RESOURCES;
	
	public int decreaseCount(int count)
	{
		if(availableResources < count)
		{
			return -1;
		}
		else
		{
			availableResources -= count;
			return 0;
		}
	}
	
	public void increaseCount(int count)
	{
		availableResources += count;
	}
} */